import React, { ChangeEvent, useMemo, useState } from 'react';
import './App.css';

function App() {
  const initMatrix: string[][] = [];
  const [rowLength, setRowLength] = useState(5);
  const [matrixToRender, setMartix] = useState(initMatrix);
  const EMPTY_STRING = '';
  const onRowChange = (event: ChangeEvent<HTMLInputElement>) => {
    const limit = +event.target.value;
    if (limit > 0) {
      setRowLength(+event.target.value);
    }
  };

  useMemo(() => {
    let matrix: string[][] = [];
    const fillMatrix = () => {
      for (let i = 0; i < rowLength; i++) {
        for (let j = 0; j < rowLength; j++) {
          matrix[i] = matrix[i] || [];
          matrix[i][j] = EMPTY_STRING;
        }
      }
    };
    fillMatrix();
    const fillRow = ({
      beginX,
      beginY,
      letftToRightDirection,
      upToDownDirection,
      axisX,
      limit,
    }: {
      beginX: number;
      beginY: number;
      letftToRightDirection: Boolean;
      upToDownDirection: Boolean;
      axisX: Boolean;
      limit: number;
    }) => {
      const getStartEnd = () => {
        if (!beginX && !beginY) {
          return [0, limit];
        }
        const begin = axisX ? beginX : beginY;
        const end = axisX
          ? letftToRightDirection
            ? begin + limit
            : begin - limit
          : upToDownDirection
          ? begin + limit
          : begin - limit;

        return [begin, end];
      };

      const [begin, end] = getStartEnd();
      const forEachLoop = (callback: (element: number) => void) => {
        const condition = (i: number): boolean => {
          return begin > end ? i > end : i < end;
        };
        for (let i = begin; condition(i); begin > end ? i-- : i++) {
          callback(i);
        }
      };
      const fillFn = (i: number) => {
        axisX ? (matrix[beginY][i] = '*') : (matrix[i][beginX] = '*');
      };
      forEachLoop(fillFn);
      if (limit < 2) {
        setMartix(matrix);
        return;
      }

      const newLimit = limit - 1;
      let newX, newY;
      if (axisX) {
        newY = upToDownDirection ? beginY + 1 : beginY - 1;
        newX = letftToRightDirection ? beginX + newLimit : beginX - newLimit;
      } else {
        newX = letftToRightDirection ? beginX + 1 : beginX - 1;
        newY = upToDownDirection ? beginY + newLimit : beginY - newLimit;
      }
      fillRow({
        beginX: newX,
        beginY: newY,
        letftToRightDirection: axisX ? !letftToRightDirection : letftToRightDirection,
        upToDownDirection: axisX ? upToDownDirection : !upToDownDirection,
        axisX: !axisX,
        limit: newLimit,
      });
    };

    fillRow({
      beginX: 0,
      beginY: 0,
      letftToRightDirection: true,
      upToDownDirection: true,
      axisX: true,
      limit: rowLength,
    });
  }, [rowLength]);

  const RenderSpiral = (): JSX.Element => {
    return (
      <div className="spiral">
        {matrixToRender.map((row) => (
          <div className="flexDiv">
            {row.map((el) => {
              if (!el) {
                return <div className="flexElement">&nbsp;</div>;
              }
              return <div className="flexElement">{el}</div>;
            })}
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="App">
      <header className="App-header">
        <input type="string" value={rowLength} onChange={onRowChange}></input>
        <RenderSpiral />
      </header>
    </div>
  );
}

export default App;
